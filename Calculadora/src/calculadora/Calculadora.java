package calculadora;

public class Calculadora {
	
	private int a;
	private int b;
	
	public Calculadora() {
		EntradaCalculadora ec = new EntradaCalculadora();
		a = ec.getA();
		b = ec.getB();
	}
	
	public int soma() {
		return this.a + this.b;
	}

}
