package calculadora;

import java.util.ArrayList;

public class EntradaCalculadora {
	
	private EditorArquivos e = new EditorArquivos("src/entrada");
	private int a;
	private int b;
	
	public EntradaCalculadora() {
		ArrayList<String> dadosLidos = e.ler();
		
		a = Integer.parseInt(dadosLidos.get(0)); // Converte a String em inteiro
		b = Integer.parseInt(dadosLidos.get(1)); // Converte a String em inteiro
		
	}
	
	public int getA() {
		return this.a;
	}
	
	public int getB() {
		return this.b;
	}
	

}
