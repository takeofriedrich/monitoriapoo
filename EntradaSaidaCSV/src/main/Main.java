package main;

import repositorio.RepositorioPessoas;
import dados.Pessoa;

public class Main {

	public static void main(String[] args) {
		
		RepositorioPessoas bancoDadosPessoas = new RepositorioPessoas();
		Pessoa p = new Pessoa("Vinicius",19,70.0f);
		if(bancoDadosPessoas.inserePessoa(p)) System.out.println("Inserido");
		if(bancoDadosPessoas.inserePessoa(p)) System.out.println("Inserido");
		if(bancoDadosPessoas.inserePessoa(p)) System.out.println("Inserido");
		
		if(bancoDadosPessoas.removePessoa(2)) System.out.println("Removido");
		if(bancoDadosPessoas.removePessoa(2)) System.out.println("Removido");
		else System.out.println("Falha");
		
	}
	
	

}
