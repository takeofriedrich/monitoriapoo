package repositorio;

import helpers.EditorArquivos;
import dados.Pessoa;
import java.util.ArrayList;

public class RepositorioPessoas {
	
	private EditorArquivos entradaSaida = new EditorArquivos("tabelas/pessoas.csv"); // Possui um atributo que contém o caminho da tabela da qual esse repositório irá fazer alterações
	private ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>(); // ArrayList de objetos desse repositório, onde todos os objetos que estavam no arquivo são carregados
	
	public RepositorioPessoas() {
	}
	
	private void carregaDados() {
		
		ArrayList<String> linhasArquivoExterno = new ArrayList<String>(); // Cria uma arrayList temporaria para armazenar o arrayList de Strings obtidos pela Classe EditorArquivos.java
		linhasArquivoExterno = this.entradaSaida.ler(); // Regasta em um ArrayList o arquivo, onde cada posição do Array contém uma linha do arquivo
		
		for(int i=0;i<linhasArquivoExterno.size();i++) { // Loop para percorrer o array que contém todas as linhas do arquivo
			
			String csvEditor = ","; // Essa String contém o separador entre uma coluna e outra da tabela do arquivo externo
			String pessoaLinha[] = linhasArquivoExterno.get(i).split(csvEditor); // Cria um array de String onde cada posição contém uma coluna de uma linha do arquivo
			
			// Essa parte é específica do Repositório de Pessoas, pois a classe Pessoa contém três atributos: nome, idade e peso
			String nome = pessoaLinha[0]; 
			int idade = Integer.parseInt(pessoaLinha[1]); // Converte o atributo que estava como String para int
			float peso = Float.parseFloat(pessoaLinha[2]); // Converte o atributo que estava como String para float
			
			Pessoa p = new Pessoa(nome,idade,peso); // Cria uma nova instância da classe pessoa com os atributos resgatados do arquivo anteriormente
			this.pessoas.add(p); // Adiciona essa instância ao Array de Pessoas (atributo dessa classe)
			
		}
	}
	
	private void gravaDados() {
		
		ArrayList<String> linhasArquivoExterno = new ArrayList<String>(); // Cria uma arrayList de Strings para ser enviado a Classe EditorArquivos para ser gravado no arquivo
		
		for(int i=0;i<pessoas.size();i++) { // Percorre todo o array de pessoas do repositório para transformar um objeto da classe Pessoa em uma string a ser "impressa" no arquivo
			String linhaAtual = ""; // Cria um objeto String vazio para incrementar um objeto do tipo Pessoa convertido em String
			Pessoa p = pessoas.get(i); // Pega a pessoa na iésima posição do array de Pessoa e armazena em um objeto temporario "p"
			linhaAtual += this.converteParaLinhaDoCSV(p); // Converte "p" para uma String pronta para ser salva no arquivo
			linhasArquivoExterno.add(linhaAtual); // Adiciona no array que será salvo no arquivo o objeto já convertido para String
			linhasArquivoExterno.add("\n"); // Adiciona a quebra de linha no array
		}
		
		entradaSaida.gravar(linhasArquivoExterno); // Grava o arrayList de linhas no arquivo
		this.limpaRepositorio(); // Exclui todos os objetos do arrayList de Pessoa pois já estão salvos na memória
		
	}
	
	private String converteParaLinhaDoCSV(Pessoa pessoa) {
		
		String pessoaLinha = ""; // Cria um objeto String vazio
		String divisorCSV = ","; // Como uma tabela CSV utiliza como separador de colunas uma virgula, definimos uma String como sendo uma virgula
		
		pessoaLinha += pessoa.getNome() + divisorCSV; // Adiciona na String que será retornada o nome da pessoa adicionado da virgula que separa as colunas do arquivo CSV
		pessoaLinha += Integer.toString(pessoa.getIdade()) + divisorCSV; // Adiciona na String que será retornada a idade da pessoa (já convertida para String) adicionado da virgula que separa as colunas do arquivo CSV
		pessoaLinha += Float.toString(pessoa.getPeso()); // Adiciona na String que será retornada o peso da pessoa (já convertida para String) adicionado da virgula que separa as colunas do arquivo CSV
		
		return pessoaLinha; // Retorna a String ou seja, o método acabou de converter um objeto pessoa, para uma String a ser inserida no arquivo
		
	}
	
	public boolean inserePessoa(Pessoa pessoa) {
		
		this.carregaDados();
		boolean resultadoInsercao = true;
		pessoas.add(pessoa);
		this.gravaDados();
		
		return resultadoInsercao;
	}
	
	public boolean removePessoa(int index) {
		
		boolean resultadoRemocao = false;
		
		this.carregaDados();
		
		if(index < pessoas.size()) {
			pessoas.remove(index);
			this.gravaDados();
			resultadoRemocao = true;
		}
		
		return resultadoRemocao;
	}
	
	private void limpaRepositorio() {
		this.pessoas.clear(); // Limpa o arrayList de pessoas
	}
	
	
	

}
