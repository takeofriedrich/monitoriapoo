package helpers;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.util.ArrayList;

public class EditorArquivos {
	
	private String caminho; // Atributo que específica o caminho onde está o arquivo a ser lido

	// Construtor do editor de arquivos, recebe como parâmetro o caminho que será usado
    public EditorArquivos(String caminho){ 
        this.caminho = caminho;
    }
    
    /*
     * Método de leitura do arquivo:
     * Esse método irá ler todo o arquivo e retornará uma arrayList de Strings, onde cada posição do array contém uma linha inteira do arquivo
     */
    public ArrayList<String> ler(){

        ArrayList<String> linhas = new ArrayList<String>(); // Cria o arrayList que será retornado pelo método

        try {
        	
          FileReader arq = new FileReader(this.caminho); // Cria um objeto que irá ler o arquivo
          BufferedReader lerArq = new BufferedReader(arq); // Cria um buffer de leitura
          String linha = lerArq.readLine(); // Cria uma String para ler linha a linha do arquivo
          
          // Entra em loop para ler o arquivo até que a String contenha null, isso significa que não há mais linhas a serem lidas e o loop é quebrado
          while (linha != null) { 
              linhas.add(linha); // Adiciona a linha recém lida no arrayList de linhas que será retornado
              linha = lerArq.readLine(); // Lê a próxima linha do arquivo (Se ela estiver em branco, a String receberá null e o loop será quebrado)
          }
          arq.close(); // Fecha o arquivo pois acabou a leitura
          
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",e.getMessage());
            System.exit(0);
        }

        return linhas; // Retorna o arrayList contendo todas as linhas recém lidas, onde cada posição representa uma delas
    }
    
    /*
     * Método de gravação do arquivo:
     * Esse método irá gravar no arquivo a arrayList que contém todas as linhas com os dados JÁ FORMATADOS, onde cada posição do array representa uma linha do arquivo
     */
    public void gravar(ArrayList<String> linhas) {
    	
    	try {
    		
    		FileWriter arq = new FileWriter(this.caminho); // Cria um objeto que irá ler o arquivo
    		
    		for(int i=0;i<linhas.size();i++) { // Cria um loop que percorre todas as posições do array que será gravado no arquivo (No caso cada posição contém uma linha já formata para ser gravada)
    			arq.write(linhas.get(i)); // Escreve no arquivo a String na posição do contador i
    		}
    		arq.close(); // Fecha o arquivo pois acabou a gravação
    		
    	} catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n",e.getMessage());
            System.exit(0);
        }

    }

}
